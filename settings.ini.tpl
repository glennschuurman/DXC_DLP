[SectionDatabase]
host: HOST
database: DB_NAME
user: DB_USER
pass: DB_PASS

[SectionSeverity]
s1: C1_PATH
s2: C2_PATH
s3: C3_PATH
s4: C4_PATH
s5: C5_PATH

[SectionSystem]
version: 0.1