#!/usr/bin/python

import argparse
import os
import platform
import logging
from Classes.hash import Hash
from Classes.sendhash import SendHash
from time import gmtime, strftime


def __main__():
    # Arguments on the command line
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", nargs="?", default="C:\Windows",
                        help="Path to be analyzed. If not declared, it will be C:\Windows")
    parser.add_argument("-csv", "--hashtocsv", help="Dumps hashes in a CSV file with hashes"
                                                    + "in the current working directory")
    parser.add_argument("-db", "--hashtodb", help="Dumps hashes into the specified database",
                        action="store_true")
    parser.add_argument("-c", "--comparison", help="Compares your hashes with the ones stored in the specified database",
                        action="store_true")
    parser.add_argument("-v", "--verbose", help="Enter debug mode of the program",
                        action="store_true")
    try:
        args = parser.parse_args()
    except Exception, e:
        logging.warning(e)
        parser.print_help()
        quit(0)

    # Clear the screen just in case it is launched from console.
    if platform.system() == "Windows":
        os.system('cls')
    else:
        os.system('clear')

    try:
        hashes = Hash()
        comparison = SendHash()
    except Exception, e:
        logging.error(e)
        parser.print_help()
        exit(0)

    # Functionality of the arguments
    if os.path.exists(args.path):
        path = args.path
    else:
        print "No valid path"
        quit(0)

    if args.verbose:
        # Set logging format
        logging.basicConfig(filename=strftime("%Y-%m-%d %H", gmtime()) + '.log', format='%(levelname)s:%(message)s',
                            level=logging.DEBUG)
    else:
        # Set logging format
        logging.basicConfig(filename=strftime("%Y-%m-%d %H", gmtime()) + '.log', format='%(levelname)s:%(message)s',
                            level=logging.ERROR)
    if args.hashtocsv:
        doc = os.path.realpath(__file__)[:-8] + args.hashtocsv
        hashes.hash_insert_csv(path, doc)
    if args.hashtodb:
        hashes.hash_insert(path)
    if args.comparison:
        comparison.compare_hash(path)

    if not args.hashtocsv and not args.hashtodb and not args.comparison:
        logging.debug("No method supplied")
        parser.print_help()


try:
    __main__()
except KeyboardInterrupt:
    quit(0)