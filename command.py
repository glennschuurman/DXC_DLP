#!/usr/bin/python
import os
import platform
import logging
from Classes.hash import Hash


# Clear the screen just in case it is launched from console.
if platform.system() == "Windows":
    os.system('cls')
else:
    os.system('clear')

# Set logging format
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
path = "C:\Windows"

#command prompt
command = ""
while command != "/exit":
    command = raw_input("Enter a Command:\n")
    if "/hashcsv" == command:
        doc = raw_input("Enter the path to the CSV where the hashes will be written:\n")
        test = Hash()
        test.hash_insert_csv(path, doc)

        print "HASHCSV FINISHED"
    elif "/hashdb" == command:
        doc = "hashes.csv"
        test = Hash()
        test.hash_insert(path)
        print "HASHDB FINISHED"

        test.hash_insert(path)
    elif "/help" == command:
        print ("/hashcsv - dumps all the hashes of your folder into a CSV\n" +
               "/hashdb - dumps all the hashes of your folder into a database\n ")
    elif command != "/exit":
        print ("This is not a valid command.\nPlease try again, write /help for more info or /exit to finish")
