from Classes.setting import Setting


class Severity:

    def __init__(self):
        self.setting = Setting().get_section_settings('SectionSeverity')

    def __del__(self):
        del self.setting

    def get_severity(self, level):
        """"
        Reads config file for Severity level
        :type level: int
        :param level: level of Severity between 1 and 5
        :return empty string when no data is found in config file
        :rtype str
        :return List with directories from that Severity level
        :rtype List
        """

        if level < 1:
            level = 1
        if level > len(self.setting):
            level = len(self.setting)
        if len(self.setting['s' + level]) == 0:
            return None
        else:
            return self.setting['s' + level].split(',')

    def get_level(self, filepath):
        """"
        Reads config file for severity, returns None if path not found, returns int of severity if found
        :type filepath: str
        :param filepath: Path to check against config file
        :return None if no match is found
        :rtype None
        :return Int of severity for that file
        :rtype int
        """

        i = 1
        while i < len(self.setting):
            j = 0
            settings_array = self.setting['s' + str(i)].split(',')
            while j < len(settings_array):
                if settings_array[j].upper() in filepath.upper():
                    return i
                j += 1
            i += 1




