import hashlib
import os
import logging
import platform

from Classes.database import Database
from Classes.severity import Severity


class Hash:
    def __init__(self):
        try:
            self._get_build_id()
            if not self.B_id:
                self._create_build()
                self._get_build_id()
            else:
                pass
        except Exception, e:
            logging.critical(e)
        self.severity = Severity()

    def __del__(self):
        pass

    def hash_insert(self, path):
        """
        Inserts all files with sha256 hashes in database
        :type path: str
        :param path: Path to directory
        """
        for dirpath, dirnames, filenames in os.walk(path):
            for filename in [f for f in filenames]:
                try:
                    filepath = os.path.join(dirpath, filename)
                    logging.debug("Proccessing : " + filepath)
                    filehash = self.hash_generator(filepath)
                    logging.debug("Hash: " + filehash)
                    self._insert(filepath, filehash)
                except IOError:
                    pass

    def hash_insert_csv(self, path, docname):
        """
        Inserts all files with sha256 hashes in a csv
        :type path: str
        :param path: Path to directory to analyze
        :type docname: str
        :param docname: Path to the CSV that will be written
        """
        try:
            doc = open(docname, 'w')
            for dirpath, dirnames, filenames in os.walk(path):
                for filename in [f for f in filenames]:
                    filepath = os.path.join(dirpath, filename)
                    filehash = self.hash_generator(filepath)
                    doc.write(str(self.B_id) + "," + filepath + "," + filehash + "\n")
        except IOError, e:
            logging.error(e)

    @staticmethod
    def _create_build():

        """"
        Creates a build_id in the database with the version of windows and the architecture
        """
        sql = "insert into Buildnumber (Buildnumber, Architecture) Values (%s, %s)"
        db = Database()
        if os.path.exists("C:\Program Files (x86)"):
            arch = "64bit"
        else:
            arch = platform.architecture()[0]
        db.amend_query(sql, [platform.version(), arch])
        del db

    def _get_build_id(self):
        """
        gets the Build_id from database, if not found self.B_id stays None otherwise self.B_id is the build_id
        """

        sql = "SELECT B_id from Buildnumber where Buildnumber = %s and Architecture = %s"
        db = Database()
        if os.path.exists("C:\Program Files (x86)"):
            arch = "64bit"
        else:
            arch = platform.architecture()[0]
        result = db.read_query(sql, [platform.version(), arch])
        del db
        try:
            self.B_id = result["B_id"]
        except TypeError, e:
            self.B_id = None

    def _insert(self, filepath, sha256_hash):
        """
        Inserts build number, filename with path and sha256 hash in database

        :type filepath: str
        :param filepath: Path to directory
        :type sha256_hash: str
        :param : Hash of the filepath file
        """

        sql = "Insert Into filehash (Priority, Filepath, Hash, B_id) Values (%s, %s, %s, %s)"
        db = Database()
        sev = self.severity.get_level(filepath)
        if not sev:
            sev = 0
        db.amend_query(sql, [sev, filepath, sha256_hash, self.B_id])
        del db

    @staticmethod
    def hash_generator(path):
        """
        generates the hash for the given path

        :type path: str
        :param path: Path to directory
        """
        BLOCKSIZE = 65536
        hasher = hashlib.sha256()
        logging.debug("Hashing: " + path)
        try:
            with open(path, 'rb') as afile:
                buf = afile.read(BLOCKSIZE)
                while len(buf) > 0:
                    hasher.update(buf)
                    buf = afile.read(BLOCKSIZE)
                filehash = hasher.hexdigest()
            del hasher
            return filehash
        except IOError, e:
            logging.error(path)
            raise IOError, e