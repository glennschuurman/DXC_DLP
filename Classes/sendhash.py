import os
import logging
import wmi
from database import Database
from hash import Hash


class SendHash:

    def __init__(self):
        self.db = Database()

    def __del__(self):
        del self.db

    def compare_hash(self, path):
        """"
        Compares file-hashes with database hashes and terminates network access if severity settings exceeded
        :type path: str
        :param path: Path to check

        """
        hasher = Hash()
        sqlPriority = "SELECT Priority FROM filehash WHERE Filepath = %s AND B_id = %s;"               # retrieves the priority value of the file
        # TODO FANCY SQL STATEMENT TO RETURN TRUE/FALSE
        sqlHash = "SELECT Hash FROM filehash WHERE Filepath = %s AND B_id = %s;"         # retrieves the hash value of the file

        try:
            for dirpath, dirnames, filenames in os.walk(path):
                for filename in [f for f in filenames]:
                    filepath = os.path.join(dirpath, filename)
                    try:
                        if hasher.hash_generator(filepath) == self.db.read_query(sqlHash,(filepath, hasher.B_id))["Hash"]:
                            pass
                        else:
                            priority = self.db.read_query(sqlPriority,(filepath, hasher.B_id))["Priority"]

                            if priority == 1:
                                logging.critical("Piority 1")
                                # function below turns of internet connection on the PC
                                # self._terminate()

                            elif priority == 2:
                                # self._terminate()
                                logging.error("Priority 2")

                            elif priority == 3:
                                logging.warning("Priority 3")

                            elif priority == 4:
                                logging.debug("Priority 4")

                            elif priority == 5:
                                logging.info("Priority 5")

                    except Exception as e:
                        logging.error(e)
                        pass
        except IOError, e:
            logging.error(e)

    @staticmethod
    def _terminate():
        """"
        Uses WMI to disable all network adapters
        """
        c = wmi.WMI()
        for interface in c.Win32_NetworkAdapter():
            if interface.NetEnabled:
                interface.Disable()

    @staticmethod
    def _restore():
        """"
        Uses WMI to restore all network adapters
        """
        c = wmi.WMI()
        for interface in c.Win32_NetworkAdapter():
            if not interface.NetEnabled:
                interface.Enable()




