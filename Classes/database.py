import pymysql.cursors
import logging
from Classes.setting import Setting


class Database:
    def __init__(self):
        self._connect()

    def __del__(self):
        try:
            self._connection.close()
        except AttributeError, e:
            logging.warning(e)

    def _connect(self):
        """
        Opens connection to the MySQL database
        """
        try:
            setting = Setting().get_section_settings('SectionDatabase')
            self._connection = pymysql.connect(host=setting['host'],
                                               user=setting['user'],
                                               password=setting['pass'],
                                               db=setting['database'],
                                               charset='utf8mb4',
                                               cursorclass=pymysql.cursors.DictCursor)
        except (pymysql.Error, AttributeError) as e:
            try:
                logging.warning("MySQL Error [%d]: %s", e.args[0], e.args[1])
            except IndexError:
                logging.warning("MySQL Error: %s", str(e))

    def read_query(self, sql, args=[]):
        """
        Fetches and returns a single MySQL Row limited by one

        :type sql: str
        :param sql: MySQL Query String
        :type args: list
        :param args: Optional arguments
        :rtype: dict
        :return: MySQL Row
        """
        result = None

        try:
            with self._connection.cursor() as cursor:
                cursor.execute(sql, args)
                result = cursor.fetchone()
        except (pymysql.Error, AttributeError) as e:
            try:
                logging.warning("MySQL Error [%d]: %s", e.args[0], e.args[1])
            except IndexError:
                logging.warning("MySQL Error: %s", str(e))

        return result

    def amend_query(self, sql, args=[]):
        """
        Execute amend MySQL Query

        :type sql: str
        :param sql: MySQL Query String
        :type args: list
        :param args: Optional arguments
        """
        try:
            with self._connection.cursor() as cursor:
                cursor.execute(sql, args)
            self._connection.commit()
        except pymysql.Error, e:
            try:
                logging.warning("MySQL Error [%d]: %s", e.args[0], e.args[1])
            except IndexError:
                logging.warning("MySQL Error: %s", str(e))
